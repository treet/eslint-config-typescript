# @treet/eslint-config

[![pipeline status][pipeline-image]][pipeline-url]

Shared [ESLint] configuration used by Treet.

[pipeline-url]: https://gitlab.com/treet/eslint-config-typescript/commits/master
[pipeline-image]: https://gitlab.com/treet/eslint-config-typescript/badges/master/pipeline.svg
[ESLint]: https://eslint.org
